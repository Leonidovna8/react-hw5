import React, { useState } from "react";
import "./CartPage.scss";
import Modal from "../../component/Modal/Modal.js"
import CheckoutForm from "./component/CartForm";
import CheckoutModal from "../../component/Modal/CheckoutModal.js";

const CartPage = ({ cart, removeFromCart }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [productIdToRemove, setProductIdToRemove] = useState(null);
    const [checkoutData, setCheckoutData] = useState(null);
    const [emptyCart, setEmptyCart] = useState([]);
    const [isOrderPlaced, setIsOrderPlaced] = useState(false);
    const [isCheckoutModalOpen, setIsCheckoutModalOpen] = useState(false); // Додайте окремий стан для CheckoutModal

    const handleRemoveFromCart = (productId) => {
        setProductIdToRemove(productId);
        setIsModalOpen(true);
    };

    const handleConfirmRemove = () => {
        if (productIdToRemove !== null) {
            removeFromCart(productIdToRemove);
            setProductIdToRemove(null);
            setIsModalOpen(false);
        }
    };

    const handleCloseModal = () => {
        setProductIdToRemove(null);
        setIsModalOpen(false);
    };

    const handleCheckout = (userData) => {
        // Опрацьовуйте дані користувача тут і виконайте необхідні дії, наприклад, очистьте кошик.
        console.log("Дані користувача:", userData);
        console.log("Придбані товари:", cart);
        // Очистити кошик тут
        localStorage.removeItem("cart");
        setCheckoutData({ userData, cart });
        setEmptyCart([]);
        setIsOrderPlaced(true);
        // Відкрити CheckoutModal
        setIsCheckoutModalOpen(true);
    };
    const handleGoToOrder = () => {
        // Додайте код для переходу на сторінку замовлення
    };

    const handleGoToCatalog = () => {
        // Додайте код для переходу до каталогу
    };


    return (
        <div>
            <h2 className="cart-title">Cart Page</h2>
            {cart.length === 0 ? (
                <p className="empty">Your cart is empty.</p>
            ) : (
                <>
                    <ul className="cart-list">
                        {cart.map((product) => (
                            <li className="cart-item" key={product.id}>
                                <div className="cart-item-details">
                                    <img
                                        className="cart-item-image"
                                        src={product.url}
                                        alt={product.name}
                                    />
                                    <div className="cart-item-info">
                                        <h3>{product.name}</h3>
                                        <p>Price: ${product.price.toFixed(2)}</p>
                                    </div>
                                </div>
                                <button className="btn-cancel" onClick={() => handleRemoveFromCart(product.id)}>
                                    Delete
                                </button>
                            </li>
                        ))}
                    </ul>
                    <CheckoutForm onSubmit={handleCheckout} />
                </>
            )}
            {productIdToRemove !== null && (
                <Modal
                    isOpen={isModalOpen}
                    onClose={handleCloseModal}
                    header="Confirm deletion"
                    text="Are you sure you want to remove this item from your cart?"
                    actions={
                        <>
                            <button className="btn-cancel" onClick={handleCloseModal}>Cancel</button>
                            <button className="btn-ok" onClick={handleConfirmRemove}>Delete</button>
                        </>
                    }
                />
            )}
            <CheckoutModal
                isOpen={isCheckoutModalOpen} // Використовуйте окрему змінну стану для CheckoutModal
                setIsOpen={setIsCheckoutModalOpen}
                onGoToOrder={handleGoToOrder}
                onGoToCatalog={handleGoToCatalog}
            />
        </div>
    );
};

export default CartPage;
