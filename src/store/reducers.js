import { createReducer } from "@reduxjs/toolkit";
import { actionProductList, actionModalOpen, actionModalClose } from "./actions.js"


const initialState = {
    productList: fetch ("products.json") .then(response => response.json()) .then(data => data),
     // Зберігаю список продуктів
    isModalOpen: false,
};

const rootReducer = createReducer(initialState, {
    [actionProductList]: (state, action) => {
        state.productList = action.payload;
    },
    [actionModalOpen]: (state) => {
        state.isModalOpen = true;
    },
    [actionModalClose]: (state) => {
        state.isModalOpen = false;
    },
});

export default rootReducer
